package ui.tree;

import model.Level2D;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Display extends JFrame {

    public Display(List<Level2D> list) throws HeadlessException {
        setSize(1000,1000);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        add(new Board(list));
    }
}
