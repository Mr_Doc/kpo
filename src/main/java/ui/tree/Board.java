package ui.tree;

import model.Level;
import model.Level2D;
import model.Node2D;

import javax.swing.*;
import java.awt.*;
import java.util.List;


public class Board extends JPanel {

    private List<Level2D> list;

    public Board(List<Level2D> list) {
        this.list = list;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawTree(g);
    }

    private void drawTree(Graphics g){
        Level2D prevLevel2D = list.get(0);
        int y = 40;
        for (Level2D level2d:list) {
            List<Node2D> node2DList = level2d.getList();
            for (Node2D node2D:node2DList) {
                g.setColor(Color.BLACK);
                g.drawOval(node2D.getX(),node2D.getY(),40,40);
                g.drawString(node2D.toString(),node2D.getX()+5,node2D.getY()+25);
                Node2D node = prevLevel2D.getNode2D(node2D.getParent());
                if (node != null){
                    g.drawLine(node2D.getX()+15,node2D.getY(),node.getX()+20,node.getY()+40);
                }
            }
            g.drawString(String.valueOf(level2d.getNumber()),950,y);
            y+=50;
            prevLevel2D = level2d;
        }
    }
}
