package exceptions;

import model.Node;

/**
 * Исключение выбрасывается при ошибках создания узла
 *
 * @author Kudiyarov Daniil
 */
public class NodeCreationException extends RuntimeException {

    /**
     * Ошибка выбрасывается если узел-родитель null
     * */
    public NodeCreationException(){
        super("Узел-родитель НЕ МОЖЕТ быть null");
    }

    public NodeCreationException(int enteredParentNodeNumber, int parentNodeNumber){
        super("Полученный номер узла-родителя {" + enteredParentNodeNumber + "} " +
                "не совпадает с порядковым номером узла-родителя {"+ parentNodeNumber + "}");
    }

    public NodeCreationException(int nodeNumber, Node parent){
        super("Порядковый номер узла {" + nodeNumber + "} уже используется в узле-родителе " + parent.getNodeCode());
    }
}
