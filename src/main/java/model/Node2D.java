package model;

public class Node2D {

    private int x;
    private int y;
    private int code;
    private int parent;

    public Node2D(int x, int y, int code, int parent) {
        this.x = x;
        this.y = y;
        this.code = code;
        this.parent = parent;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getCode() {
        return code;
    }

    public int getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return this.code + "-" + this.parent;
    }
}
