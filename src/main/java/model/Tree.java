package model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс описывает дерево, хранящее в себе уровни с узлами
 *
 * @author Kudiyarov Daniil
 */
@Getter
@NoArgsConstructor
public class Tree {

    /**
     * Список уровней в дереве
     */
    private List<Level> levels;
    /**
     * Максимальное возможное число исходящих узлов из узла
     */
    private int maxAmountOfChildNodes;
    /**
     * Количество висячих узлов
     */
    private int hangingNodeAmount;
    /**
     * Количество всех узлов
     */
    private int totalNodeAmount;
    /**
     * Характеризует правильность построения дерева
     */
    private boolean valid = false;
    /**
     * Характеризует способ генерации числа узла-детей
     * true - генерируется рандомное число узлов-детей
     * false - число узлов-детей будет постоянным
     */
    private boolean isRandom = false;

    /**
     * Генерирование дерева с рандомным количеством исходящих узлов.
     * Общее количество сгенерированных узлов может отличаться от введённого значения в большую сторону
     *
     * @param maxChildNodeAmount Количество исходящих узлов -1 {@link Tree#maxAmountOfChildNodes}
     * @param nodeAmount         Общее количество узлов
     * @param isRandom           Способ генерации узлов-детей {@link Tree#isRandom}
     * @see Tree#generateRandomOutgoingNodeNumber()
     */
    public void generateTree(int maxChildNodeAmount, int nodeAmount, boolean isRandom) {
        this.maxAmountOfChildNodes = maxChildNodeAmount - 1;
        this.isRandom = isRandom;
        this.totalNodeAmount = 1;
        Level currentLevel = getFirstLevel();
        while (this.totalNodeAmount <= nodeAmount) {
            Level nextLevel = new Level(currentLevel.getLevelNumber() + 1, currentLevel.getMaxNodeNumber());
            List<Node> parentNodes = currentLevel.getNodeList();
            if (parentNodes.size() == 0) {
                setTreeValidation();
                return;
            }
            for (Node parentNode : parentNodes) {
                int childNodeAmount = getChildNodeAmount();
                for (int i = 1; i <= childNodeAmount; i++) {
                    Node node = new Node(parentNode, nextLevel.getMaxNodeNumber() + 1);
                    nextLevel.addNode(node);
                    this.totalNodeAmount++;
                }
            }
            //Добавляем уровень в дерево если есть хотя бы один узел на уровне
            //Если узлов 0, то отнимаем висячие узлы этого уровня от общего числа узлов
            if (nextLevel.getNodeCount() != 0) {
                this.levels.add(nextLevel);
            } else {
                this.hangingNodeAmount -= parentNodes.size();
            }
            currentLevel = nextLevel;
        }
        this.valid = true;
    }

    /**
     * Метод возвращает число узлов-детей исходя из способа создания дерева
     * от булевской переменной {@link Tree#isRandom}
     *
     * @return число узлов-детей
     */
    private int getChildNodeAmount() {
        int childNodeAmount;
        if (this.isRandom) {
            childNodeAmount = generateRandomOutgoingNodeNumber();
            if (childNodeAmount == 0) {
                this.hangingNodeAmount++;
            }
        } else {
            childNodeAmount = this.maxAmountOfChildNodes;
        }
        return childNodeAmount;
    }

    /**
     * Данный метод проверяет является ли дерево валидным
     * Если узлов в дереве >= 10, то дерево считается построенно правильно
     * Если узлов в дереве < 10, то дерево считается построенно не правильно
     */
    private void setTreeValidation() {
        this.valid = this.totalNodeAmount >= 10;
    }

    /**
     * Метод создаёт первый уровень дерева
     *
     * @see Level
     * @return первый уровень со вложенным первым узлов
     */
    private Level getFirstLevel() {
        this.levels = new ArrayList<>();
        final Level firstLevel = new Level(1, 1);
        final Node firstNode = new Node();
        firstLevel.addNode(firstNode);
        this.levels.add(firstLevel);
        return firstLevel;
    }

    /**
     * Метод генерирует рандомное число исходящих узлов в диапазоне от 0 до {@link Tree#maxAmountOfChildNodes}
     *
     * @return сгенерированное рандомное число
     */
    private int generateRandomOutgoingNodeNumber() {
        return (int) (Math.random() * maxAmountOfChildNodes);
    }

    /**
     * Метод вычисляет альфа-значение
     *
     * @return альфа значение
     */
    public double getAlpha() {
        return (double) this.totalNodeAmount / this.hangingNodeAmount;
    }

    @Override
    public String toString() {
        return "Tree{Всего узлов:" + this.totalNodeAmount +
                ", Висячих узлов:" + this.hangingNodeAmount +
                ", Альфа-значение:" + String.format("%.2f",getAlpha()) +
                ", Рандомный способ генерации:" + this.isRandom;
    }
}
