package model;

import java.util.ArrayList;
import java.util.List;

public class Level2D {

    private int number;
    private List<Node2D> list = new ArrayList<>();

    public Level2D(int number) {
        this.number = number;
    }

    public void addNode2D(Node2D node2D){
        list.add(node2D);
    }

    public int getNumber() {
        return number;
    }

    public List<Node2D> getList() {
        return list;
    }

    public Node2D getNode2D(int parentCode){
        for (Node2D node:list) {
            if (node.getCode() == parentCode){
                return node;
            }
        }
        return null;
    }
}
