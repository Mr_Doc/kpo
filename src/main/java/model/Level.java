package model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс описывает уровень, хранящий в себе узлы
 *
 * @author Kudiyarov Daniil
 */
@Getter
@Setter
public class Level {

    /**
     * Порядковый номер уровня
     */
    private final int levelNumber;
    /**
     * Максимальный порядковый номер узла на данном уровне
     */
    private int maxNodeNumber;
    /**
     * Количество узлов на уровне
     */
    private int nodeCount;
    /**
     * Список узлов, хранящихся на уровне
     */
    private List<Node> nodeList;

    /**
     * Конструктор создания нового уровня
     *
     * @param levelNumber   порядковый номер уровня
     * @param maxNodeNumber максимальный порядковый номер узла
     */
    public Level(int levelNumber, int maxNodeNumber) {
        this.levelNumber = levelNumber;
        this.maxNodeNumber = maxNodeNumber;
        this.nodeList = new ArrayList<>();
    }

    /**
     * Добавление узла на уровень
     *
     * @param node Добавляемый узел
     */
    public void addNode(Node node) {
        this.nodeList.add(node);
        this.nodeCount++;
        if (node.getNodeNumber() > this.maxNodeNumber) {
            this.maxNodeNumber = node.getNodeNumber();
        }
    }

    @Override
    public String toString() {
        return "Level -> " + this.levelNumber +
                "\n Node amount -> " + this.nodeCount;
    }
}
