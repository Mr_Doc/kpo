package model;

import exceptions.NodeCreationException;
import lombok.Getter;

/**
 * Класс описывает узел дерева
 *
 * @author Kudiyarov Daniil
 */
@Getter
public class Node {

    /**
     * Узел-родитель данного узла
     */
    private final Node parent;
    /**
     * Порядковый номер узла
     */
    private final int nodeNumber;
    /**
     * Порядковый номер узла-родителя
     */
    private final int parentNodeNumber;

    /**
     * Данный конструктор используется ТОЛЬКО для создания самого первого узла
     */
    public Node() {
        this.parent = null;
        this.nodeNumber = 1;
        this.parentNodeNumber = 0;
    }

    /**
     * Конструктор используется для создания узлов не являющимся самым первым
     *
     * @param parent     Узел-родитель
     * @param nodeNumber Порядковый номер узла
     * @see Node#validateData(Node)
     */
    public Node(Node parent, int nodeNumber) {
        validateData(parent);
        this.parent = parent;
        this.nodeNumber = nodeNumber;
        this.parentNodeNumber = parent.getNodeNumber();
    }

    /**
     * Проверка валидности данных для создания узла
     *
     * @param parent Узел-родитель
     */
    private void validateData(Node parent) {
        if (parent == null) {
            throw new NodeCreationException();
        }
    }


    /**
     * @return Код узла в строковом представлении
     */
    public String getNodeCode() {
        return this.nodeNumber + "-" + this.parentNodeNumber;
    }


}
