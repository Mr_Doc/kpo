import model.*;
import ui.tree.Display;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int i = 1;
        boolean isRandom = true;
        List<Tree> list = new ArrayList<>();
        long start = System.currentTimeMillis();
        while (list.size() < 100){
            Tree tree = new Tree();
            while (!tree.isValid()){
                tree.generateTree(4,50,isRandom);
                i++;
            }
            list.add(tree);
        }
        System.out.println(System.currentTimeMillis() - start);
        System.out.println("I->" + i);

        int x = 20;
        int y = 20;
        int amount = 0;
        List<Level> levels = list.get(0).getLevels();
        List<Level2D> level2DList = new ArrayList<>();
        Level2D firstLevel = new Level2D(1);
        firstLevel.addNode2D(new Node2D(500,20,1,0));
        for (Level level:levels) {
            List<Node> nodes = level.getNodeList();
            Level2D level2D = new Level2D(level.getLevelNumber());
            int nodeNumber = nodes.size();
            int step = 750/(nodeNumber+1);
            for (Node node:nodes) {
                Node2D node2D = new Node2D(x+step,y,node.getNodeNumber(),node.getParentNodeNumber());
                level2D.addNode2D(node2D);
                x+=step;
                amount++;
            }
            level2DList.add(level2D);
            x = 20;
            y+=50;
            if (!isRandom && amount >20){
                break;
            }
        }
        level2DList.remove(0);
        level2DList.add(0,firstLevel);

        EventQueue.invokeLater(()->{
            Display display = new Display(level2DList);
            display.setVisible(true);
        });
    }

}
